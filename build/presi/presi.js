app.presi = function ($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {

    $scope.dropDownP = function () {
        $('.drop-down-box').toggleClass('drop-down-active');
    }

    $scope.closeDropDownP = function () {
        $('.drop-down-box').removeClass('drop-down-active');
    }


    // open/close terminal
    $scope.openTerminalP = function () {
        $('.presi-terminal').removeClass('modal-hide');
    }

    $scope.closeTerminalP = function () {
        $('.presi-terminal').addClass('modal-hide');
    }

    // open/close shared
    $scope.openSharedP = function () {
        $('.presi-shared').removeClass('modal-hide');
    }

    $scope.closeSharedP = function () {
        $('.presi-shared').addClass('modal-hide');
    }

    // open/close songs
    $scope.openSongsP = function () {
        $('.presi-songs').removeClass('modal-hide');
    }

    $scope.closeSongsP = function () {
        $('.presi-songs').addClass('modal-hide');
    }

    // open/close songs
    $scope.openMailP = function () {
        $('.presi-mail').removeClass('modal-hide');
    }

    $scope.closeMailP = function () {
        $('.presi-mail').addClass('modal-hide');
    }

    $scope.inbox = true;
    $scope.send = false;

    $scope.inboxBtn = function () {
        $scope.inbox = true;
        $scope.send = false;
    }

    $scope.sendBtn = function () {
        $scope.inbox = false;
        $scope.send = true;
    }

    if($location.path() == '/presi') {
        $timeout(function(){
            jQuery(function ($, undefined) {
                $('#terminal').terminal(function (command) {
                    if (command !== '') {
                        try {
                            var result = window.eval(command);
                            if (result !== undefined) {
                                this.echo(new String(result));
                            }
                        } catch (e) {
                            this.error(new String(e));
                        }
                    } else {
                        this.echo('');
                    }
                }, {
                        greetings: '',
                        name: 'js',
                        height: 220,
                        prompt: 'Newrocorp>'
                    });
            });
        }, 1000);
    }

};

