app.loginFirebase = function ($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {
  var auth = $firebaseAuth();

  $scope.signIn = function () {
    if ($scope.emailTxt == 'mark_rothschild' || $scope.emailTxt == 'Mark_Rothschild') {
      auth.$signInWithEmailAndPassword($scope.emailTxt + '@roomescape.com', $scope.passTxt + 'mark')
        .then(function (firebaseUser) {

          console.log('Usuario: ' + firebaseUser.email + ' conectado');
          $location.path('/vice');

        }).catch(function (error) {
          $scope.error = error;

          if (error) {
            $('.login-box .box').addClass('wiggle');
            $timeout(function () {
              $('.login-box .box').removeClass('wiggle');
            }, 300);
          }
        });
    } else if ($scope.emailTxt == 'werner_heisenberg' || $scope.emailTxt == 'Werner_Heisenberg') {
      auth.$signInWithEmailAndPassword($scope.emailTxt + '@roomescape.com', $scope.passTxt + '00')
        .then(function (firebaseUser) {

          console.log('Usuario: ' + firebaseUser.email + ' conectado');
          $location.path('/presi');

        }).catch(function (error) {
          $scope.error = error;

          if (error) {
            $('.login-box .box').addClass('wiggle');
            $timeout(function () {
              $('.login-box .box').removeClass('wiggle');
            }, 300);
          }
        });
    } else {
      auth.$signInWithEmailAndPassword($scope.emailTxt + '@roomescape.com', $scope.passTxt)
        .then(function (firebaseUser) {
          console.log('Usuario: ' + firebaseUser.email + ' conectado');

          if ($scope.emailTxt == 'Guest' || $scope.emailTxt == 'guest') {
            $location.path('/guest');
          } else if ($scope.emailTxt == 'jose_garcia' || $scope.emailTxt == 'Jose_Garcia') {
            $location.path('/security');
          }

        }).catch(function (error) {
          $scope.error = error;

          if (error) {
            $('.login-box .box').addClass('wiggle');
            $timeout(function () {
              $('.login-box .box').removeClass('wiggle');
            }, 300);
          }
        });
    }
  };

}