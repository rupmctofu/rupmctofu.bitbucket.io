app.security = function ($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {

    $scope.dropDownS = function () {
        $('.drop-down-box').toggleClass('drop-down-active');
    }

    $scope.closeDropDownS = function () {
        $('.drop-down-box').removeClass('drop-down-active');
    }


    // open/close terminal
    $scope.openTerminalS = function () {
        $('.security-terminal').removeClass('modal-hide');
    }

    $scope.closeTerminalS = function () {
        $('.security-terminal').addClass('modal-hide');
    }

    // open/close documents
    $scope.openDocumentsS = function () {
        $('.security-documents').removeClass('modal-hide');
    }

    $scope.closeDocumentsS = function () {
        $('.security-documents').addClass('modal-hide');
    }

    // open/close shared
    $scope.openSharedS = function () {
        $('.security-shared').removeClass('modal-hide');
    }

    $scope.closeSharedS = function () {
        $('.security-shared').addClass('modal-hide');
    }

    // open/close songs
    $scope.openSongsS = function () {
        $('.security-songs').removeClass('modal-hide');
    }

    $scope.closeSongsS = function () {
        $('.security-songs').addClass('modal-hide');
    }
    
    // open/close documents
    $scope.openCalendar = function () {
        $('.security-calendar').removeClass('modal-hide');
    }

    $scope.closeCalendar = function () {
        $('.security-calendar').addClass('modal-hide');
    }

    // open/close fichas
    $scope.openFichaS = function ($event) {
        $('.ficha').removeClass('modal-hide');

        //get this info
        var img = angular.element($event.currentTarget).children('.hidden-info').children('img').attr('src');
        var name = angular.element($event.currentTarget).children('p').text();
        var full = angular.element($event.currentTarget).children('.hidden-info').children('.this-full').text();
        var birth = angular.element($event.currentTarget).children('.hidden-info').children('.this-birth').text();
        var residence = angular.element($event.currentTarget).children('.hidden-info').children('.this-residence').text();
        var occupation = angular.element($event.currentTarget).children('.hidden-info').children('.this-occupation').text();
        var criminal = angular.element($event.currentTarget).children('.hidden-info').children('.this-criminal').text();
        var mental = angular.element($event.currentTarget).children('.hidden-info').children('.this-mental').text();
        var conflict = angular.element($event.currentTarget).children('.hidden-info').children('.this-conflict').text();
        var number = angular.element($event.currentTarget).children('.hidden-info').children('.this-number').text();

        //apply this info to modal
        $('.modal-img').attr('src', img);
        $('.modal-name').text(name);
        $('.modal-full').text(full);
        $('.modal-birth').text(birth);
        $('.modal-residence').text(residence);
        $('.modal-occupation').text(occupation);
        $('.modal-criminal').text(criminal);
        $('.modal-mental').text(mental);
        $('.modal-conflict').text(conflict);
        $('.modal-number').text(number);
    }

    $scope.closeFichaS = function () {
        $('.ficha').addClass('modal-hide');
    }

    $scope.fichas = [
        {
            img: '../../assets/img/dani.png',
            name: 'Daniel',
            full: 'Villagrasa Rosario',
            birth: '30/12/1989',
            residence: 'Olivella',
            occupation: 'Gardener',
            criminal: 'Yes',
            mental: 'Yes',
            conflict: '85%',
            number: '4232'
        },
        {
            img: '../../assets/img/jose.png',
            name: 'Jose',
            full: 'Beteta Garcia',
            birth: '30/12/1989',
            residence: 'Olivella',
            occupation: 'Gardener',
            criminal: 'Yes',
            mental: 'Yes',
            conflict: '85%',
            number: '4233'
        },
        {
            img: '../../assets/img/marcos.png',
            name: 'Marcos',
            full: 'Beteta Garcia',
            birth: '30/12/1989',
            residence: 'Olivella',
            occupation: 'Gardener',
            criminal: 'Yes',
            mental: 'Yes',
            conflict: '85%',
            number: '4234'
        },
        {
            img: '../../assets/img/jonay.png',
            name: 'Jonay',
            full: 'Garcia Leyva',
            birth: '30/12/1989',
            residence: 'Olivella',
            occupation: 'Gardener',
            criminal: 'Yes',
            mental: 'Yes',
            conflict: '85%',
            number: '4235'
        },
        {
            img: '../../assets/img/manu.png',
            name: 'Manuel',
            full: 'Garcia Fernandez',
            birth: '30/12/1989',
            residence: 'Olivella',
            occupation: 'Gardener',
            criminal: 'Yes',
            mental: 'Yes',
            conflict: '85%',
            number: '4236'
        },
        {
            img: '../../assets/img/jesus.png',
            name: 'Jesus M.',
            full: 'Villamizar Castellano',
            birth: '30/12/1989',
            residence: 'Olivella',
            occupation: 'Gardener',
            criminal: 'Yes',
            mental: 'Yes',
            conflict: '85%',
            number: '4237'
        }
    ]
};

