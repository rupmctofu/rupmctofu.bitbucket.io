app.guest = function ($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {

    $scope.dropDown = function () {
        $('.drop-down-box').toggleClass('drop-down-active');
    }

    $scope.closeDropDown = function () {
        $('.drop-down-box').removeClass('drop-down-active');
    }

    // open/close terminal
    $scope.openTerminal = function () {
        $('.guest-terminal').removeClass('modal-hide');
    }

    $scope.closeTerminal = function () {
        $('.guest-terminal').addClass('modal-hide');
    }

    // open/close documents
    $scope.openDocuments = function () {
        $('.guest-documents').removeClass('modal-hide');
    }

    $scope.closeDocuments = function () {
        $('.guest-documents').addClass('modal-hide');
    }
    // open/close documents
    $scope.openReadme = function () {
        $('.guest-readme').removeClass('modal-hide');
    }

    $scope.closeReadme = function () {
        $('.guest-readme').addClass('modal-hide');
    }

    // open/close documents
    $scope.openPrint = function () {
        $('.guest-print').removeClass('modal-hide');
        $timeout(function(){
            $('.print-title').empty().text('Print complete');
            $('.progress-wrapper, .print-name').hide();
        }, 5000);
    }

    $scope.closePrint = function () {
        $('.guest-print').addClass('modal-hide');
    }

    $scope.playPause = function () {
        var videoPlayer = document.getElementById('video-box');

        if (videoPlayer.paused) {
            videoPlayer.play();
        }
        else {
            videoPlayer.pause();
        }
    }

    $scope.openVideo = function () {
        $('.guest-video').removeClass('modal-hide');
    }

    $scope.closeVideo = function () {
        $('.guest-video').addClass('modal-hide');
        var videoPlayer = document.getElementById('video-box');
        videoPlayer.pause();
    }

    // open/close fichas
    $scope.openFicha = function ($event) {
        $('.ficha').removeClass('modal-hide');

        //get this info
        var img = angular.element($event.currentTarget).children('.hidden-info').children('img').attr('src');
        var name = angular.element($event.currentTarget).children('p').text();
        var full = angular.element($event.currentTarget).children('.hidden-info').children('.this-full').text();
        var birth = angular.element($event.currentTarget).children('.hidden-info').children('.this-birth').text();
        var residence = angular.element($event.currentTarget).children('.hidden-info').children('.this-residence').text();
        var occupation = angular.element($event.currentTarget).children('.hidden-info').children('.this-occupation').text();
        var criminal = angular.element($event.currentTarget).children('.hidden-info').children('.this-criminal').text();
        var mental = angular.element($event.currentTarget).children('.hidden-info').children('.this-mental').text();
        var conflict = angular.element($event.currentTarget).children('.hidden-info').children('.this-conflict').text();
        var number = angular.element($event.currentTarget).children('.hidden-info').children('.this-number').text();

        //apply this info to modal
        $('.modal-img').attr('src', img);
        $('.modal-name').text(name);
        $('.modal-full').text(full);
        $('.modal-birth').text(birth);
        $('.modal-residence').text(residence);
        $('.modal-occupation').text(occupation);
        $('.modal-criminal').text(criminal);
        $('.modal-mental').text(mental);
        $('.modal-conflict').text(conflict);
        $('.modal-number').text(number);
    }

    $scope.closeFicha = function () {
        $('.ficha').addClass('modal-hide');
    }

    $scope.fichas = [
        {
            img: '../../assets/img/dani.png',
            name: 'Daniel',
            full: 'Villagrasa Rosario',
            birth: '30/12/1989',
            residence: 'Olivella',
            occupation: 'Gardener',
            criminal: 'Yes',
            mental: 'Yes',
            conflict: '85%',
            number: '4232'
        },
        {
            img: '../../assets/img/jose.png',
            name: 'Jose',
            full: 'Beteta Garcia',
            birth: '30/12/1989',
            residence: 'Olivella',
            occupation: 'Gardener',
            criminal: 'Yes',
            mental: 'Yes',
            conflict: '85%',
            number: '4233'
        },
        {
            img: '../../assets/img/marcos.png',
            name: 'Marcos',
            full: 'Beteta Garcia',
            birth: '30/12/1989',
            residence: 'Olivella',
            occupation: 'Gardener',
            criminal: 'Yes',
            mental: 'Yes',
            conflict: '85%',
            number: '4234'
        },
        {
            img: '../../assets/img/jonay.png',
            name: 'Jonay',
            full: 'Garcia Leyva',
            birth: '30/12/1989',
            residence: 'Olivella',
            occupation: 'Gardener',
            criminal: 'Yes',
            mental: 'Yes',
            conflict: '85%',
            number: '4235'
        },
        {
            img: '../../assets/img/manu.png',
            name: 'Manuel',
            full: 'Garcia Fernandez',
            birth: '30/12/1989',
            residence: 'Olivella',
            occupation: 'Gardener',
            criminal: 'Yes',
            mental: 'Yes',
            conflict: '85%',
            number: '4236'
        },
        {
            img: '../../assets/img/jesus.png',
            name: 'Jesus M.',
            full: 'Villamizar Castellano',
            birth: '30/12/1989',
            residence: 'Olivella',
            occupation: 'Gardener',
            criminal: 'Yes',
            mental: 'Yes',
            conflict: '85%',
            number: '4237'
        }
    ]
};

