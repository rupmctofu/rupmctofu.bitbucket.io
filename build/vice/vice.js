app.vice = function ($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {

    $scope.dropDownV = function () {
        $('.drop-down-box').toggleClass('drop-down-active');
    }

    $scope.closeDropDownV = function () {
        $('.drop-down-box').removeClass('drop-down-active');
    }


    // open/close terminal
    $scope.openTerminalV = function () {
        $('.vice-terminal').removeClass('modal-hide');
    }

    $scope.closeTerminalV = function () {
        $('.vice-terminal').addClass('modal-hide');
    }

    // open/close shared
    $scope.openSharedV = function () {
        $('.vice-shared').removeClass('modal-hide');
    }

    $scope.closeSharedV = function () {
        $('.vice-shared').addClass('modal-hide');
    }

    // open/close songs
    $scope.openSongsV = function () {
        $('.vice-songs').removeClass('modal-hide');
    }

    $scope.closeSongsV = function () {
        $('.vice-songs').addClass('modal-hide');
    }

    // open/close songs
    $scope.openMailV = function () {
        $('.vice-mail').removeClass('modal-hide');
    }

    $scope.closeMailV = function () {
        $('.vice-mail').addClass('modal-hide');
    }

    $scope.inbox = true;
    $scope.send = false;

    $scope.inboxBtn = function(){
        $scope.inbox = true;
        $scope.send = false;
    }

    $scope.sendBtn = function(){
        $scope.inbox = false;
        $scope.send = true;
    }
};

