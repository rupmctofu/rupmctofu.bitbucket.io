//app name
var app = angular.module('app', ['ngRoute', 'ngAnimate', 'firebase']);

//firebase auth
const config = {
    apiKey: "AIzaSyCtzjV9FIN7KDwsOYg9uy93L64GJP2mogU",
    authDomain: "https://rupmctofu.bitbucket.io/",
    databaseURL: "https://roomescape-kactus.firebaseio.com",
    projectId: "roomescape-kactus",
    storageBucket: "",
    messagingSenderId: "670575734170"
};

firebase.initializeApp(config);

app.factory("Auth", ["$firebaseAuth", function ($firebaseAuth) {
    return $firebaseAuth();
}]);

// !auth redirections
app.run(["$rootScope", "$location", function ($rootScope, $location) {
    $rootScope.$on("$routeChangeError", function (event, next, previous, error) {
        if (error === "AUTH_REQUIRED") {
            $location.path("/login");
        }
    });
}]);


//on-enter=""
app.directive('onEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.onEnter);
                });

                event.preventDefault();
            }
        });
    };
});

//$locations
app.config(['$routeProvider', function ($routeProvider) {

    $routeProvider
        .when('/login', {
            templateUrl: './build/login/login.html',
            controller: 'appController',
        })
        .when('/guest', {
            templateUrl: './build/guest/guest.html',
            controller: 'appController',
            // !auth ? auth
            resolve: {
                "currentAuth": ["Auth", function (Auth) {
                    return Auth.$requireSignIn();
                }]
            }
        })
        .when('/security', {
            templateUrl: './build/security/security.html',
            controller: 'appController',
            // !auth ? auth
            resolve: {
                "currentAuth": ["Auth", function (Auth) {
                    return Auth.$requireSignIn();
                }]
            }
        })
        .when('/vice', {
            templateUrl: './build/vice/vice.html',
            controller: 'appController',
            // !auth ? auth
            resolve: {
                "currentAuth": ["Auth", function (Auth) {
                    return Auth.$waitForSignIn();
                }]
            }
        })
        .when('/presi', {
            templateUrl: './build/presi/presi.html',
            controller: 'appController',
            // !auth ? auth
            resolve: {
                "currentAuth": ["Auth", function (Auth) {
                    return Auth.$waitForSignIn();
                }]
            }
        })
        .otherwise({
            //404?
            redirectTo: '/login',
        });

}]);

//js source
app.controller('appController', ['$scope', '$location', '$window', '$timeout', '$firebaseObject', '$firebaseArray', '$firebaseAuth', '$firebaseStorage', function ($scope, $location, $window, $timeout, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage) {
    
    //login
    app.login($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    app.loginFirebase($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    
    //guest
    app.guest($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    app.guestFirebase($scope, $location, $timeout,$window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);

    //security
    app.security($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    app.securityFirebase($scope, $location, $timeout,$window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    
    //vice
    app.vice($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    app.viceFirebase($scope, $location, $timeout,$window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);

    //presi
    app.presi($scope, $location, $timeout, $window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    app.presiFirebase($scope, $location, $timeout,$window, $firebaseObject, $firebaseArray, $firebaseAuth, $firebaseStorage);
    
}]);